﻿using System;
using System.Collections;
using Calculator;
using NUnit.Framework;

namespace nunittest
{
    [TestFixture]
    public class CalculatorTest
    {
        private Calculator.Calculator _calculator;

        [SetUp]
        public void Setup_Calculator()
        {
            _calculator = new Calculator.Calculator();
        }

        [TearDown]
        public void TearDown_Calculator()
        {
            _calculator = null;
        }

        [TestCase(1, 9, 10)]
        [TestCase(2, 3, 5)]
        [TestCase(88, 5, 93)]
        public void Should_SuccessToAdd(decimal i, decimal j, decimal k)
        {
            Assert.AreEqual(_calculator.Add(i, j), k);
        }

        [TestCaseSource(typeof(CalculatorTestProvider), "SubtractSource")]
        public void Should_SuccessToSubtract(decimal i, decimal j, decimal k)
        {
            Assert.AreEqual(_calculator.Subtract(i, j), k);
        }

        [TestCaseSource(typeof(CalculatorTestProvider), "MultiplySource")]
        public void Should_SuccessToMultiply(decimal i, decimal j, decimal k)
        {
            Assert.AreEqual(_calculator.Multiply(i, j), k);
        }

        [TestCaseSource(typeof(CalculatorTestProvider), "DivideSource")]
        public void Should_SuccessToDivide(decimal i, decimal j, decimal k)
        {
            Assert.AreEqual(_calculator.Divide(i, j), k);
        }

        [TestCaseSource(typeof(CalculatorTestProvider), "DivideWithZeroSource")]
        public void When_DivisionByZero_Then_ExpectedBehavior(decimal i, decimal j, decimal k)
        {
            Assert.Throws(typeof(DivideByZeroException), delegate
            {
                _calculator.Divide(i, j);
            });
        }
    }

    public static class CalculatorTestProvider
    {
        public static IEnumerable SubtractSource()
        {
            yield return new decimal[] {5, 2, 3};
            yield return new decimal[] {10, 5, 5 };
            yield return new decimal[] { 6, 9, -3 };
        }

        public static IEnumerable MultiplySource()
        {
            yield return new decimal[] { 5, 2, 10 };
            yield return new decimal[] { 10, -5, -50 };
            yield return new decimal[] { 6, 9, 54 };
        }

        public static IEnumerable DivideWithZeroSource()
        {
            yield return new decimal[] { 5, 0, 0 };
            yield return new decimal[] { 10, 0, 0 };
            yield return new decimal[] { 6, 0, 0 };
        }

        public static IEnumerable DivideSource()
        {
            yield return new decimal[] { 5, 1, 5 };
            yield return new decimal[] { 10, 1, 10 };
            yield return new decimal[] { 6, 1, 6 };
        }
    }
}
