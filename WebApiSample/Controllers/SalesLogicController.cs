﻿using System;
using Calculator;
using System.Web.Http;

namespace WebApiSample.Controllers
{
    public class SalesLogicController : ApiController 
    {
        [HttpGet]
        public IHttpActionResult GetSalesLogic()
        {
            Calculator.Calculator calculator = new Calculator.Calculator();
            calculator.Add(5, 5);
            return Ok("Get Successfully.");
        }

        [HttpPost]
        public IHttpActionResult PostSalesLogic()
        {
            Calculator.Calculator calculator = new Calculator.Calculator();
            calculator.Subtract(5, 5);
            return Ok("Posted.");
        }

        [HttpPut]
        public IHttpActionResult PutSalesLogic()
        {
            Calculator.Calculator calculator = new Calculator.Calculator();
            calculator.Multiply(5, 5);
            return Ok("Updated.");
        }

        [HttpDelete]
        public IHttpActionResult DeleteSalesLogic()
        {
            Calculator.Calculator calculator = new Calculator.Calculator();
            calculator.Divide(5, 5);
            return Ok("Deleted.");
        }
    }
}
