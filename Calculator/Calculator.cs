﻿using System;
namespace Calculator
{
    public class Calculator
    {
        public decimal Add(decimal i, decimal j)
        {
            return i + j;
        }

        public decimal Subtract(decimal i, decimal j)
        {
            return i - j;
        }

        public decimal Multiply(decimal i, decimal j)
        {
            return i * j;
        }

        public decimal Divide(decimal i, decimal j)
        {
            return i / j;
        }
    }
}
